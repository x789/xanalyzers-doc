# Configuration
Some analyzers are configurable. To use your own settings, add a file called _xanalyzers.json_ to your project's root folder and set its property _"Build Action"_ to _"C# analyzer additional file"_.
The configuration file contains a [JSON](https://www.json.org/) encoded _settings_ object that contains analyzer-specific settings. See the help file of each analyzer for its configuration.
## Example
```javascript
{
  "settings": {
    "XA201": [
	  {"ancestorOf": "System.Exception", "requiresBaseIn": ["ACME.BaseException"]}
	]
  }
}
```
___
(c) 2019 x789 - Handmade with ❤️

Part of [xAnalyzers](https://www.nuget.org/packages/xAnalyzers/) - [Report an issue](https://bitbucket.org/x789/xanalyzers/issues)